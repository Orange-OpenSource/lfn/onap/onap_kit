# onap_kit

Ansible roles using onap_tests git project to launch VNF onboarding and instantiation.  
The master branch is linked to the master branch of onap-tests (Frankfurt release)
A specific branch is available for ONAP ElAlto version with right ONAP testing
configuration`onap-testing.j2`.  

Prerequisites
-------------
Python version 3 shall be used (onap_tests requirement) => To check with python -V  

Copy user_cloud.yml in vars directory to provide information related to your tenant.  
**user_cloud.yml example**  
>
```
clouds:
    training_user:
        auth:
            auth_url: https://vim.pod4.opnfv.fr:5000
            password: ---to fill---
            project_name: openlab-vnfs
            project_id: 5906b9b8fd9642df9ba1c9e290063439
            user_domain_name: Default
            username: training_user
        compute_api_version: '2.15'
        identity_api_version: '3'
        interface: public
        project_domain_name: Default
        region_name: RegionOne
```  

Inventory file under inventory directory or .ssh/config file shall be updated to be compatible and to address jumphost in the target cloud environment.    
**Extract .ssh/config with jumphost defined in inventory file**    
>
```
    host jumphost  
        HostName 10.4.2.x  
        User debian  
        ProxyJump onap.pod4.opnfv.fr  
   host onap.pod4.opnfv.fr  
       User toto  
       ProxyCommand /usr/bin/connect -S goodway.rd.francetelecom.fr:1080 %h %p
```

Onboarding Execution
---------------------
To update the vars file:
* global.yaml  
  Set HTTP_PROXY values (install_http to access internet repositories, sock_http to reach cloud environment)  
  Restriction: keep OPNFV for CloudOwner because of existing issue in onap_tests)
* service.yml  
  It must be overwritten to define your service values
  * service name = Service_name+service_number  
  * Customer and vendor

As examples, two roles are predefined for onboarding: onboard and onboard_NOF. The role "onboard" allows to onboard an ubuntu VM.
The role onboard_NOF allows to onboard a service composed of two VNFs.  

These directories can be duplicated or overwritten to create your own roles for your service.  

To define the service TestVNF composed of a packet generator and a VNF to test, the role onboard_NOF can be used.
* A service yml file *TestVNF-service.j2* allowing to overwrite the default values of env file of your heat package shall be defined based on NOFtest-service.j2. In this jinja file, variables to be included in vars/service.yml can be used for templating purpose (See the example of key_value or service_name).
* The heat package of each VNF shall be copied under roles/onboard_NOF/package. It will be then copied in the right ONAP_tests directory (onap_tests/templates/heat_files/) executing onboar_NOF role. The roles tasks are defined in main.yaml located under roles/onboard_NOF/tasks. Don't forget to  overwrite the different paths of tasks named "Copy heat artifacts" and take care to use the same VNF name in the different files.
* File vars/service.yml to update with new service_name, service_number and perhaps new variables

Ansible command under onap_kit directory to run the ONAP onboarding:  
> *ansible-playbook -i inventory/inventory onboardNOF.yaml*

To see the logs during the execution:
> *tail -f onap_tests.debug.log*

Instantiation Execution
-----------------------

As all service variables were defined to execute onboarding, the following ansible just needs to be run:  
> *ansible-playbook -i inventory/inventory instantiate.yaml*

CNF onboarding and Instantiation over k8s
--------------------------
For the onboarding, the vars files `service.yml` and `global.yml` shall be updated.  
The service_name shall contains the pattern `k8s` (example: nginxk8s) to trigger an
instantiation on kubernetes environment.  
The package shall be build and store under roles/onboard/packages (See nginxk8s
example)  

> *ansible-playbook -i inventory/inventory onboard.yaml*

The following command needs to be launch to set onap environment (if not done at installation):  

* Declare k8s region and register it within multicloud
* POST K8s connectivity information on multicloud (Link onap to a kubernetes environment)

By default, the so must be configured to support multicloud and the region shall be declared (Installation part)  
This command uses the FQDN jumphost whose resolution shall be set in the .ssh/config file.  

```shell
ansible-playbook -i inventory/inventory prepare_k8s.yaml --extra-vars "@vars/global.yml" --extra-vars "@vars/service.yml"
```

The usual command to launch the instantiation remains:

> *ansible_playbook -i inventory/inventory instantiate.yaml*
